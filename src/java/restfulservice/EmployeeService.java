/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restfulservice;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author TrongTran
 */
@Path("/employee")
public class EmployeeService {

    @PersistenceContext(unitName = "RestfulAPIPU")
    private EntityManager em;

    @Path("/getall")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Employee> getAll() {
        return em.createNamedQuery("Employee.findAll").getResultList();
    }

   
    @Path("/add/{id}/{name}")
    @Consumes("application/json")
    public String createEmployee(@PathParam("id") String id, @PathParam("name") String name) {
        Employee employee = new Employee();
        employee.setId(id);
        employee.setName(name);
        em.persist(employee);
        return "add success";
    }

    @Path("/update/{id}/{name}")
    @Consumes("application/json")
    public String updateEmployee(@PathParam("id") String id, @PathParam("name") String name) {
        Employee employee = new Employee();
        employee.setId(id);
        employee.setName(name);
        em.merge(employee);
        return "update success";
    }
}
